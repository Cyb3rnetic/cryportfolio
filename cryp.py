"""
Simple program to calculate your portfolio values.

Reference:
    https://www.cryptocompare.com/api/

Written in Python3.5
"""

__author__ = 'Cyb3rnetic'
__copyright__ = 'Copyleft (c) 2017'
__license__ = 'GNU GPL'


import getopt, sys, requests, signal, json


"""
Colors
"""
W = '\033[0m'                   # white (normal)
R = '\033[31m'                  # red
G = '\033[32m'                  # green
O = '\033[33m'                  # orange
B = '\033[34m'                  # blue
P = '\033[35m'                  # purple
C = '\033[36m'                  # cyan
K = '[{}*{}]'.format(G, W)      # gui ok
F = '[{}*{}]'.format(R, W)      # gui fail


line = "\n{}-------------------------------------------------------------\n".format(K)

if "raw_input" not in dir(__builtins__):
    raw_input = input


def exit_gracefully(signum, frame):
    signal.signal(signal.SIGINT, original_sigint)

    try:
        if raw_input("\nReally quit? (y/n): ").lower().startswith('y'):
            sys.exit(1)

    except KeyboardInterrupt:
        print("Ok ok, quitting")
        sys.exit(1)

    # restore the exit gracefully handler here
    signal.signal(signal.SIGINT, exit_gracefully)


def usage(code):
    """
    usage
    :param code: 

    Displays application usage information
    """
    print('{} ------------------------------------------------------------'.format(K))
    print('{} {} [options]'.format(K, sys.argv[0],))
    print('{} ------------------------------------------------------------'.format(K))
    print('{} --btc <total>                   : View Bitcoin values'.format(K))
    print('{} --eth <total>                   : View Ethereum values'.format(K))
    print('{} --xrp <total>                   : View Ripple values'.format(K))
    print('{} --ltc <total>                   : View Litecoin values'.format(K))
    print('{} --aeon <total>                  : View Aeon values'.format(K))
    print('{} --dsh <total>                   : View Dashcoin values'.format(K))
    print('{} --bcn <total>                   : View Bytecoin values'.format(K))
    print('{} --xmr <total>                   : View Monero values'.format(K))
    print('{} ------------------------------------------------------------'.format(K))
    print('{} Examples'.format(K, sys.argv[0], ))
    print('{} ------------------------------------------------------------'.format(K))
    print('{} clear && python3.5 cryp.py --ltc=2.00087271'.format(K))
    print('{} clear && python3.5 cryp.py --aeon=10 --xrp=32 --eth=0.1 --ltc=2.00087271 --btc=0.00004039'.format(K))
    print("\n")
    sys.exit(code)


def main(argv):
    """
    main
    :param argv:

    Initial function of the application
    Handle command opts and args in argv and act accordingly
    """
    print("{}{} {} - Crypto Currency Portfolio Value Tracker {}".format(line, K, sys.argv[0], line))

    total = 0

    try:
        opts, args = getopt.getopt(argv, "h", ["btc=",
                                               "eth=",
                                               "xrp=",
                                               "ltc=",
                                               "aeon=",
                                               "dsh=",
                                               "bcn=",
                                               "xmr="
                                                ])

    except getopt.GetoptError:
        usage(2)
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage(0)
            sys.exit()
        elif opt in ("--btc"):
            amt = float(arg)
            values = json.loads(get("https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD"))
            total = total + (float(values['USD'])*amt)
            print("{} {} BTC {} -> {} {} {} -> PER: {} {} {} -> Portfolio USD: {} {} {}".format(K, G, W, O, str(amt), W, G, str(values['USD']), W, G, (str(float(values['USD'])*amt)), W))
        elif opt in ("--eth"):
            amt = float(arg)
            values = json.loads(get("https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD"))
            total = total + (float(values['USD']) * amt)
            print("{} {} ETH {} -> {} {} {} -> PER: {} {} {} -> Portfolio USD: {} {} {}".format(K, G, W, O, str(amt), W, G, str(values['USD']), W, G, (str(float(values['USD'])*amt)), W))
        elif opt in ("--xrp"):
            amt = float(arg)
            values = json.loads(get("https://min-api.cryptocompare.com/data/price?fsym=XRP&tsyms=USD"))
            total = total + (float(values['USD']) * amt)
            print("{} {} XRP {} -> {} {} {} -> PER: {} {} {} -> Portfolio USD: {} {} {}".format(K, G, W, O, str(amt), W, G, str(values['USD']), W, G, (str(float(values['USD'])*amt)), W))
        elif opt in ("--ltc"):
            amt = float(arg)
            values = json.loads(get("https://min-api.cryptocompare.com/data/price?fsym=LTC&tsyms=USD"))
            total = total + (float(values['USD']) * amt)
            print("{} {} LTC {} -> {} {} {} -> PER: {} {} {} -> Portfolio USD: {} {} {}".format(K, G, W, O, str(amt), W, G, str(values['USD']), W, G, (str(float(values['USD'])*amt)), W))
        elif opt in ("--aeon"):
            amt = float(arg)
            values = json.loads(get("https://min-api.cryptocompare.com/data/price?fsym=AEON&tsyms=USD"))
            total = total + (float(values['USD']) * amt)
            print("{} {} AEON {} -> {} {} {} -> PER: {} {} {} -> Portfolio USD: {} {} {}".format(K, G, W, O, str(amt), W, G, str(values['USD']), W, G, (str(float(values['USD'])*amt)), W))
        elif opt in ("--dsh"):
            amt = float(arg)
            values = json.loads(get("https://min-api.cryptocompare.com/data/price?fsym=DSH&tsyms=USD"))
            total = total + (float(values['USD']) * amt)
            print("{} {} DSH {} -> {} {} {} -> PER: {} {} {} -> Portfolio USD: {} {} {}".format(K, G, W, O, str(amt), W, G, str(values['USD']), W, G, (str(float(values['USD'])*amt)), W))
        elif opt in ("--bcn"):
            amt = float(arg)
            values = json.loads(get("https://min-api.cryptocompare.com/data/price?fsym=BCN&tsyms=USD"))
            total = total + (float(values['USD']) * amt)
            print("{} {} BCN {} -> {} {} {} -> PER: {} {} {} -> Portfolio USD: {} {} {}".format(K, G, W, O, str(amt), W, G, str(values['USD']), W, G, (str(float(values['USD'])*amt)), W))
        elif opt in ("--xmr"):
            amt = float(arg)
            values = json.loads(get("https://min-api.cryptocompare.com/data/price?fsym=XMR&tsyms=USD"))
            total = total + (float(values['USD']) * amt)
            print("{} {} XMR {} -> {} {} {} -> PER: {} {} {} -> Portfolio USD: {} {} {}".format(K, G, W, O, str(amt), W, G, str(values['USD']), W, G, (str(float(values['USD'])*amt)), W))

    print("\n{} {} TOTAL {} -> Portfolio USD: {} {} {}\n".format(K, G, W, G,total, W))


def get(query):
    """
    get
    :param query: 
    :return: 

    Perform a GET request 
    """
    r = requests.get(query)
    return r.text


if __name__ == "__main__":
    """
    init

    Initialize the script application's main function
    """
    original_sigint = signal.getsignal(signal.SIGINT)
    signal.signal(signal.SIGINT, exit_gracefully)
    main(sys.argv[1:])

